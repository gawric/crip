<?php
require_once('controller/interface/ICalculate.php');

class CalculateProductionController  implements ICalculate
{
		//Хранилище куриц
	private ChickenProductionModel $chickenProductionModel;
	private KineProductionModel $kineProductionModel;
	
	public function __construct($chickenModel , $kineModel)
    {
	  
	  $this->chickenProductionModel = $chickenModel;
	  $this->kineProductionModel = $kineModel;
	 
    }
   
   public function  calc()
   {
	  
		echo "+++++++++Общее количество собранной продукции:+++++++++";
		echo "<br>";
		echo "Дата получения продукции:  "; echo "<b>"; echo date("Y-m-d H:i:s"); echo "</b>";
		echo "<br>";
		echo "Всего собрано молока: "; echo "<b>"; echo $this->kineProductionModel->getAllMilk(); echo " литров</b>";
		echo "<br>";
		echo "Всего собрано яиц:";  echo "<b>"; echo $this->chickenProductionModel->getAllEgg(); echo " штук</b>";
		echo "<br>";
		echo "+++++++++Общее количество собранной продукции:+++++++++";
   }
}