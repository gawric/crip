<?php
require_once('controller/interface/ICollect.php');
require_once('controller/supportCowShed/ReadCowShed.php');

class CollectProduction implements ICollect
{
	private  $chickenArr;
	private  $kineArr;
	private  ReadCowShed $readcow;
	
	public function __construct($chickenArr , $kineArr)
    {
	
		$this->chickenArr = $chickenArr;
		$this->kineArr = $kineArr;
		
		$this->readcow = new ReadCowShed();
    }
   
   public function chickenCollect(): ChickenProductionModel
   {
		return $this->readcow->collectAllChicken($this->chickenArr);
   }
   
    public function kineCollect(): KineProductionModel
   {
		return $this->readcow->collectAllKine($this->kineArr);
   }
   
  
}