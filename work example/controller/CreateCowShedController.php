<?php
require_once('controller/interface/ICowShed.php');
require_once('model/СhickenModel.php');
require_once('model/KineModel.php');
require_once('controller/supportCowShed/GenCowShed.php');
require_once('controller/supportCowShed/ReadCowShed.php');

class CreateCowShedController implements ICowShed
{
	//Хранилище куриц
	private $chickenArr;
	private $kineArr;
	
	public function __construct()
    {
		$this->chickenArr = array();
		$this->kineArr = array();
    } 
	
	
	public function createChicken()
	{
		$gencow = new GenCowShed();
		return $gencow->genChiken($this->chickenArr);
	}
	
	public function createKine()
	{
		$gencrip = new GenCowShed();
		return $gencrip->genKine($this->kineArr);
	}
   
   
  
}