<?php
require_once('model/production/ChickenProductionModel.php');
require_once('model/production/KineProductionModel.php');

class ReadCowShed
{
	private const chickenEgg = "countEgg";
	private const litersMilk = "litersMilk";
	
	public function collectAllChicken($chickenArr)
	{	
		return $this->collectChicken($chickenArr);
    }
	
	public function collectAllKine($kineArr)
	{	
	   return $this->collectKine($kineArr);
    }
	
	//Общая функция для сбора
	private function collectChicken($collectArr) : ChickenProductionModel{
	
		
		$productionModel = new ChickenProductionModel();
		$allEgg = $this->forCollect(self::chickenEgg , $collectArr);
		
		$productionModel->setAllEgg($allEgg);

		
		return $productionModel;
	}
	

	
	private function collectKine($collectArr) : KineProductionModel{
	
		$productionModel = new KineProductionModel();
		$allMilk = $this->forCollect(self::litersMilk , $collectArr);
		
		$productionModel->setAllMilk($allMilk);
		

		
		return $productionModel;
	}
	
	private function forCollect($keyId , $collectArr)
	{
		$productionModel = 0;
		
		//восстановление из масива
		foreach ($collectArr as $v) 
		{
		
			foreach ($v as $keyT => $value)
			{
				if($keyT == $keyId)
				{
					$productionModel = $productionModel + $value;
				}
			
			}
			
		}
		return $productionModel;
	}
}