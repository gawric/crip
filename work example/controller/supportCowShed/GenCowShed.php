<?php
class GenCowShed
{
	public function genChiken($chickenArr): Array
    {
		return $this->forGen($chickenArr , 10 , true);
    }
   
   public function genKine($chickenKine): Array
   {
		return $this->forGen($chickenKine , 20 , false);
	}
	
	private function forGen($arr , $size , $chiken)
	{
		for ($i = 0; $i < $size; $i++)
		{
			if($chiken == true)
			{
				
				$arr = $this->insertArrChicken($i , $arr);
			}
			else
			{
				$arr = $this->insertArrKine($i , $arr);
			}
			
		}
		
		return $arr;
	}
	
	private function insertArrChicken($i , $arr)
	{
		$strval = strval($i) ;
		
			$str = 'Chicken' . $strval;
			$model = (array)$this->createModelChiken($i , $str  , $this->genEgg()); 
			
			$arr[$i] = $model;
			
		return $arr;
	}
	
	private function insertArrKine($i  , $arr)
	{
		$strval = strval($i) ;
		
			$str = 'Kine' . $strval;
			$model = (array)$this->createModelKine($i , $str  , $this->genLiters()); 
			
			$arr[$i] = $model;
			
		return $arr;
	}
   
    private function createModelKine($id , $name , $count)
	{
	   $model = new KineModel;
			$model->setId($id);
			$model->setNameKine($name);
			$model->setCountMilk($count);
			
	    return $model;
	}
   
   
   private function createModelChiken($id , $name , $count)
   {
	   $model = new СhickenModel;
			$model->setId($id);
			$model->setNameChiken($name);
			$model->setCount($count);
			
	    return $model;
   }
   
   private function genEgg()
   {
	   return rand(0, 1);
   }
   
   private function genLiters()
   {
	   return rand(8, 12);
   }
}
   