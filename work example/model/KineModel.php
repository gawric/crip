<?php
class KineModel
{
	//к сожелению private поля не передаются в массив только public
	public $id;
	public $nameKine;
	public $litersMilk;
	//set
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setNameKine($name)
	{
		$this->nameKine = $name;
	}
	
	public function setCountMilk($count)
	{
		$this->litersMilk = $count;
	}
	
	
	
	//get
	public function getId()
	{
		return $this->id;
	}
	
	public function getKine()
	{
		return $this->nameKine;
	}
	
	public function getCountMilk()
	{
		return $this->litersMilk;
	}
}