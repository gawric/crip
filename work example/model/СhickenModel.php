<?php
class СhickenModel
{
	public $id;
	public $nameChiken;
	public $countEgg;
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setNameChiken($name)
	{
		$this->nameChiken = $name;
	}
	
	public function setCount($count)
	{
		$this->countEgg = $count;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getChiken()
	{
		return $this->nameChiken;
	}
	
	public function getCountEgg()
	{
		return $this->countEgg;
	}
}